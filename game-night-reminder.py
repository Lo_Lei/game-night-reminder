import os
import sys
from datetime import datetime
import pymsteams

webhook_url = os.environ.get("GAME_NIGHT_MST_URL")
if not webhook_url:
    raise Exception("webhook URL not defined")

week_number = datetime.today().isocalendar()[1]
if week_number % 2 == 0:
    print("reminder should only be sent on odd weeks")
    sys.exit(0)

myTeamsMessage = pymsteams.connectorcard(webhook_url)
myTeamsMessage.text("@Game Night Reminder: Morgen ist Game Night! 🕹️🎮")
myTeamsMessage.send()
